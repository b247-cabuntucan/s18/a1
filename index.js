/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function



	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


// Activity #1

console.log("Adding numbers: 5, 7");
function addTwoNumbers(x, y) {
    sum = x + y;
    console.log(sum);
}

addTwoNumbers(5, 7);

console.log("Subacting numbers: 12, 8");
function subtractTwoNumbers(x, y) {
    difference = x - y;
    console.log(difference);
}

subtractTwoNumbers(12, 7);


// Activity 2

console.log("Multiply numbers: 6, 7");
function multiplyTwoNumbers(x, y){
    return x * y;
}

product = multiplyTwoNumbers(6,7);
console.log(product);

console.log("Divide numbers: 12, 6");
function divideTwoNumbers(x, y){
    return x / y;
}

quotient = divideTwoNumbers(12,6);
console.log(quotient);


// Activity 3

console.log("Finding the Area of Circle with raduis: 5");
function areaCircle(r){
    
    return 3.1416 * r ** 2;
}

areaOfCircle = areaCircle(5);
console.log(areaOfCircle);

// Activity 4

console.log("Finding the average of 4 numbers: 5, 8, 9, 7");
function findAverage(d1, d2, d3, d4){
    return (d1+d2+d3+d4) / 4;
}

averageOfFourNumbers = findAverage(5, 8, 9, 7);
console.log(averageOfFourNumbers);

// Activity 5

console.log("Find if the score is passing.");
function isPassingScore(score, totalScore){
    
    percentage = score / totalScore;

    return percentage >= .75;
}

console.log("74 out of 100:");
remark = isPassingScore(74, 100)
console.log(remark);

console.log("65 out of 80:");
remark = isPassingScore(65, 80)
console.log(remark);

console.log("13 out of 20:");
remark = isPassingScore(13, 20)
console.log(remark);